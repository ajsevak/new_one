class SavingsAccount
  include Comparable
  def initialize(starting_balance)
    @balance = starting_balance
  end
  def balance
    @balance
  end
  def balance(new_amount)
    @balance = new_amount
  end

  def <=> (other)
    self.balance <=>other.balance
  end
end

  a = SavingsAccount.new 100
  b =  SavingsAccount.new 200
  c =  SavingsAccount.new 300

account = [a,b,c]
account.each do |acct|
  puts "Account value is: #{acct.balance} "
end
account.sort!

account.each do |acct|
  puts "Account value is: #{acct.balance} "
end