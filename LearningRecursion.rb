class Fixnum
  def factorial
   self <= 1? 1 : self * (self -1).factorial

  end
end
        puts 5.factorial
class Numeric
  def euros; self * 1.292; end
end

puts 20.euros

class Numeric
  @@currencies = {'yen' => 0.013, 'euro' => 1.292, 'rupee' => 0.019}
  def method_missing(method_id, *args, &block) #capture all args
    singular_currency = method_id.to_s.gsub(/s$/,'')
    if @@currencies.has_key?(singular_currency)
      self * @@currencies[singular_currency]
   else
    super
   end
  end
  def self.add_currency(name,conversion)
    @@currencies[name.to_s] = conversion
  end

end

puts 20.yen

puts 100.peso
